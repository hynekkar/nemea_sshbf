import pytrap
import datetime

from detector.constants import ftrs
#feature_cols = ['duration', 'num_pkts_in', 'num_pkts_out', 'bytes_in', 'bytes_out', 'dp_9_bytes', 'dp_10_bytes', 'dp_11_bytes', 'dp_12_bytes'];



class featureExtractor:
    def prealocateResultList():
        # create a list with 9 elements (zeros)
        result = 9 * [0]
        return result;

    def extract(recievedFlow):
        result = featureExtractor.prealocateResultList();
        result[ftrs.Duration] = float(recievedFlow.TIME_LAST) - float(recievedFlow.TIME_FIRST)
        result[ftrs.NumPktsIN] = recievedFlow.PACKETS;
        result[ftrs.NumPktsOUT] = recievedFlow.PACKETS;
        result[ftrs.BytesIN] = recievedFlow.BYTES;
        result[ftrs.BytesOUT] = recievedFlow.BYTES;

        maxPcktIndex = len(recievedFlow.PPI_PKT_LENGTHS) - 1;
        resultIndex = ftrs.DP9Bytes;

        for recievedIndex in range(8, min(maxPcktIndex,11)):
            result[resultIndex] = recievedFlow.PPI_PKT_LENGTHS[recievedIndex];
            resultIndex = resultIndex + 1;

        return [result]

