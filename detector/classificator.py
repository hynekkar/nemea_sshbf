import pickle
from sklearn import tree
import datetime
import pytrap
from threading import Lock



class DBEntry:
    def __init__(self):
        self.occurence = 1;
        self.lastOccurence = datetime.datetime.now();

    def addOccurence(self):
        self.occurence = self.occurence + 1;
        self.lastOccurence = datetime.datetime.now();

    def getOccurences(self):
        return self.occurence;

    def getLastOccurence(self):
        return self.lastOccurence;


class BFLoginClassificator:
    def __init__(self, model, maxFailedLogins, InvalidationInterval):
        self.clf = model;
        self.maxFailed = maxFailedLogins;
        self.InvalidationInterval = InvalidationInterval;
        self.db = dict();
        self.dbMutex = Lock();

    def classify(self, sourceAddr, featureVector):
        predVector = self.clf.predict_proba(featureVector);
        with self.dbMutex:
            if(predVector[0][1] == 1):
                if sourceAddr in self.db:
                    self.db[sourceAddr].addOccurence;
                else:
                    self.db[sourceAddr] = DBEntry();

                if(self.db[sourceAddr].getOccurences() > self.maxFailed):
                    return True;
            else:
                if sourceAddr in self.db:
                    del self.db[sourceAddr];

        return False;

    def getEntry(self, sourceAddr):
        if sourceAddr in self.db:
            return self.db[sourceAddr];
        return None;

    def cleanOldRecords(self):
        print("Cleaning:")
        with self.dbMutex:
            for key, value in self.db:
                if (datetime.datetime.now() - value.getLastOccurence(self)) > InvalidationInterval:
                    del self.db[key];
