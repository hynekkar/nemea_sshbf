import sys
import os
import sqlite3
import pandas as pd
import numpy as np
import argparse
import pickle
import graphviz

from sklearn import tree  # Import Decision Tree Classifier
from sklearn.model_selection import train_test_split # Import train_test_split function
from sklearn import metrics #Import scikit-learn metrics module for accuracy calculatio
from sklearn.model_selection import cross_val_score


def extant_file(x):
    """
    'Type' for argparse - checks that file exists but does not open.
    """
    if not os.path.exists(x):
        # Argparse uses the ArgumentTypeError to give a rejection message like:
        # error: argument input: x does not exist
        raise argparse.ArgumentTypeError("{0} does not exist".format(x))
    return x

argpar = argparse.ArgumentParser(description="Create pickled ML model from provided SQLite database.")
argpar.add_argument('-p', '--picture', help="Output pdf file with trained decision tree model visualisation.")

requiredArgs = argpar.add_argument_group('required arguments');

requiredArgs.add_argument('-i','--input' , type=extant_file,
                    help="SQLite database with dataset.", required=True);
requiredArgs.add_argument('-o', '--output', type=argparse.FileType('wb'),
                    help="Output file with pickled model.", required=True);

arguments = argpar.parse_args();

conn = sqlite3.connect(arguments.input);
cursor = conn.cursor();
df = pd.read_sql_query("SELECT * from flows", conn);
conn.close();

feature_cols = ['duration', 'num_pkts_in', 'num_pkts_out', 'bytes_in', 'bytes_out', 'dp_9_bytes', 'dp_10_bytes', 'dp_11_bytes', 'dp_12_bytes'];
X = df[feature_cols];
y = df['bruteforce'];

clf = tree.DecisionTreeClassifier(max_depth=10, min_samples_split=10, min_samples_leaf=10);
clf.fit(X,y);
arguments.output.write(pickle.dumps(clf));

print("Using 5-fold cross validation...");
scores = cross_val_score(clf, X, y, cv=5);
print("Estimated classifier accuracy: ", np.mean(scores));


if arguments.picture :
    dot_data = tree.export_graphviz(clf, out_file=None, feature_names=feature_cols, class_names=["False", "True"]);
    graph = graphviz.Source(dot_data);
    graph.render(arguments.picture);
