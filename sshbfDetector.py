#!/usr/bin/env python3

import pytrap
import sys, os
import pickle
import datetime
import threading
import time
import signal
import argparse

from sklearn import tree

#USER_CLASSES
from detector.classificator import BFLoginClassificator
from detector.featureExtractor import featureExtractor

garbageThreadTimer = None;
run = True;

def garbageFunc(classificator):
    classificator.cleanOldRecords
    global garbageThreadTimer;
    garbageThreadTimer = threading.Timer(60, garbageFunc, args=classificator);
    garbageThreadTimer.start();

def SignalHandler(signum, frame):
    print("SSHBF exit");
    if(garbageThreadTimer != None):
        garbageThreadTimer.cancel();
    global run;
    run = False;

def extant_file(x):
    """
    'Type' for argparse - checks that file exists but does not open.
    """
    if not os.path.exists(x):
        # Argparse uses the ArgumentTypeError to give a rejection message like:
        # error: argument input: x does not exist
        raise argparse.ArgumentTypeError("{0} does not exist".format(x))
    return x


signal.signal(signal.SIGINT, SignalHandler);
signal.signal(signal.SIGTERM, SignalHandler);

argpar = argparse.ArgumentParser(description="Create pickled ML model from provided SQLite database.")
argpar.add_argument('-r', '--retries', type=int, help="reporting treshold value for failed login. Default value is 3.")
argpar.add_argument('-t', '--validinterval', type=int, help="new record valid duration in seconds. Default value is 3600 (1 hour)")
requiredArgs = argpar.add_argument_group('required arguments');
requiredArgs.add_argument('-m', '--model', type=extant_file,
                     help="input pickled decision tree model", required=True);
requiredArgs.add_argument('-i', '--ifcspec',
                     help="Specification of interface types and their parameters. For more help\
                      see https://nemea.liberouter.org/trap-ifcspec/", required=True);
arguments = argpar.parse_args();

#initialization of pytrap a
trap = pytrap.TrapCtx()
trap.init(["-i", arguments.ifcspec], 1, 1) #sys.argv

#initialization of classificator
model = pickle.load(open(arguments.model, "rb"));
retryTreshold = 3 if arguments.retries == None else arguments.retries;
timeDeltaInSeconds = 3600 if arguments.validinterval == None else arguments.ValidInterval;
clf = BFLoginClassificator(model, retryTreshold, datetime.timedelta(seconds=timeDeltaInSeconds));

#initialization of garbageFunction
garbageFunc(clf);


# Set the list of required fields in received messages.
# This list is an output of e.g. flow_meter - basic flow.
inputspec = "ipaddr DST_IP,ipaddr SRC_IP,uint64 BYTES,uint64 LINK_BIT_FIELD,time TIME_FIRST,time TIME_LAST,uint32 PACKETS,uint16 DST_PORT,uint16 SRC_PORT,uint8 DIR_BIT_FIELD,uint8 PROTOCOL,uint8 TCP_FLAGS,uint8 TOS,uint8 TTL,uint16* PPI_PKT_LENGTHS"
trap.setRequiredFmt(0, pytrap.FMT_UNIREC, inputspec)
rec = pytrap.UnirecTemplate(inputspec)

# Define a template of alert (can be extended by any other field)
alertspec = "ipaddr SRC_IP,ipaddr DST_IP,uint16 DST_PORT,uint16 SRC_PORT,time EVENT_TIME,uint16 ATTEMPTS"
alert = pytrap.UnirecTemplate(alertspec)
# set the data format to the output IFC
trap.setDataFmt(0, pytrap.FMT_UNIREC, alertspec)

# Allocate memory for the alert, we do not have any variable fields
# so no argument is needed.
alert.createMessage()

#clf.classify(rec.SRC_IP, feature_col);
def do_detection(rec):
    global alert, c

    # don't process flows that are too short
    if rec.PACKETS < 11:
        return

    if(clf.classify(rec.SRC_IP, featureExtractor.extract(rec))):
        alert.SRC_IP = rec.SRC_IP
        alert.DST_IP = rec.DST_IP
        alert.SRC_PORT = rec.SRC_PORT
        alert.DST_PORT = rec.DST_PORT
        alert.EVENT_TIME = rec.TIME_FIRST
        entry = clf.getEntry(rec.SRC_IP);
        if entry != None:
            alert.ATTEMPTS = entry.getOccurences();
        else:
            alert.ATTEMPTS = 0;
        # send alert
        trap.send(alert.getData(), 0)



# Main loop
while run:
    try:
        data = trap.recv()
    except pytrap.FormatChanged as e:
        fmttype, inputspec = trap.getDataFmt(0)
        rec = pytrap.UnirecTemplate(inputspec)
        data = e.data
    if len(data) <= 1:
        SignalHandler(0,0);
    rec.setData(data)

    do_detection(rec)

# Free allocated TRAP IFCs
trap.finalize()
